<?php namespace esapimini;  
//namespace gracianStree\infrastructure\security;       
echo " - - - - - - - - - - -  - - - - - - - -  - - - - - - - -  - - - - - - - - - - - - - \n"; 
echo " encoderApp \n";


include_once('./EncoderProxy.php');   

use esapimini\EncoderProxy;

$proxy = new EncoderProxy();   
  
$input = 'no encoding: ../';  // no encoding  
$input = 'hexadecimal encoding:  %2E%2E%2f'; // hexadecimal encoding
$input = 'double encoding: %252E%252E%252F'; // double encoding     
$r = $proxy->canonicalize($input);
if(isset($r['error'])){
   print_r($r); 
}else{
   echo "\n" . $r . "\n"; 
}

