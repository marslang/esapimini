<?php namespace esapimini;       

/*
$esapi_php =  realpath(__DIR__ . '/ESAPI.php'); 
include_once  $esapi_php;           

include_once 'src/reference/GracianEncoder.php';    
include_once 'src/reference/DefaultEncoder.php';
*/
//use gracianStree\domain\securityPorts\EncoderInterface;      

use esapimini\ESAPI;
use esapimini\reference\GracianEncoder;         
use esapimini\reference\DefaultEncoder;    
use esapimini\errors\IntrusionException;       

class EncoderProxy
{       
    private $encoder = null;
    private $error_list = null;   
    public $data = null;            
    
    //_____________________________________________________________________________________________
    public function __construct(){   
        // global $ESAPI is nodig want deze truc in de hele esapi code gebruikt    
        //echo \Config::get('project.esapi_xml'); exit();
        //$ESAPI = new ESAPI(\Config::get('project.esapi_xml'));      
        $esapi_xml =  realpath(__DIR__ . '/../ESAPI.xml');   
        //echo  'esapi_xml = ' . $esapi_xml . "\n";    
        
        global $ESAPI;      
        $ESAPI = new ESAPI($esapi_xml);   
        //ESAPI::setEncoder(new GracianEncoder());  // voor canonicalize     
        ESAPI::setEncoder(new DefaultEncoder());  // voor canonicalize
        
        $this->encoder = ESAPI::getEncoder();    
        
   
    }
   
   /**  
    * $input = a user input value
    * $result = an array with a default key: result['input']  , which can be the cleanen $input, or an emty string
    * when it is an empty string, then there is also a result['error']
    *
   */ 
    public function canonicalize($input){            
        /*
         $input = 'no encoding: ../';  // no encoding  
         $input = 'hexadecimal encoding:  %2E%2E%2f'; // hexadecimal encoding
         $input = 'double encoding: %252E%252E%252F'; // double encoding      
        */
         //echo "\n" . 'input before = ' . $input . "\n";    
         $result = array('input' => '');
         try{
             $result['input'] = $this->encoder->canonicalize($input);   
             //echo '-x-'.$input.'-x-';
         }catch(IntrusionException $e){  
             // TODO: throw hier een gewone exception
             //echo "\n --- error returned. --- \n";   
             $result['error'] = array(
                 'userMessage' => $e->getUserMessage(), 
                 'logMessage' => $e->getLogMessage()    
             );  
         }
         return $result;   
    }

    public function getErrorList(){
        
    }    
    
}